package com.gradoservice.forgeideaplugin.project_components

import com.gradoservice.forgeideaplugin.application_components.MyApplicationComponent
import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.intellij.openapi.components.AbstractProjectComponent
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.WindowManager
import java.awt.event.WindowEvent
import java.awt.event.WindowFocusListener
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.progress.impl.BackgroundableProcessIndicator


open class ProjectComponent protected constructor(project: Project) : AbstractProjectComponent(project) {
    private var projectFocusListener: WindowFocusListener? = null

    override fun projectOpened() {
        MyLogger.info("Open ${this.myProject}")
        val projectFrame = WindowManager.getInstance().getFrame(myProject)
        projectFocusListener = object : WindowFocusListener {
            override fun windowGainedFocus(e: WindowEvent) {
                MyLogger.info("Gain $myProject")
                MyApplicationComponent.getInstance().setFocused(myProject)
                update(myProject)
            }

            override fun windowLostFocus(e: WindowEvent) {
                MyLogger.info("Lost $myProject")
            }
        }
        projectFrame!!.addWindowFocusListener(projectFocusListener)
    }

    override fun projectClosed() {
        MyLogger.info("Closed ${this.myProject}")
        val projectFrame = WindowManager.getInstance().getFrame(myProject)
        projectFrame?.removeWindowFocusListener(projectFocusListener)
    }


    fun update(project: Project) {
        val task = object : Task.Backgroundable(project, "Forge Plugin") {
            override fun run(indicator: ProgressIndicator) {
                indicator.text = "Update account"
                val account = AccountManager.updateAccount(myProject)
//                indicator.text = "Load current issue"
//                account?.currentIssueId?.let { IssuesManager.load(it) }
            }
        }
        val progressIndicator = BackgroundableProcessIndicator(task)
        ProgressManager.getInstance().runProcessWithProgressAsynchronously(task
                , progressIndicator
        )
//        ApplicationManager.getApplication().executeOnPooledThread {
//            ApplicationManager.getApplication().runReadAction {
//                Thread.sleep(5000)
//            }
//        }
    }

}
