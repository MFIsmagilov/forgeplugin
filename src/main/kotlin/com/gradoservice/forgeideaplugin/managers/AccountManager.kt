package com.gradoservice.forgeideaplugin.managers

import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.models.Account
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent

import com.intellij.openapi.project.Project
import com.taskadapter.redmineapi.bean.Issue
import com.taskadapter.redmineapi.bean.User

object AccountManager {


    private val FORGE_ACCOUNT_ACCOUNT_ID = "ForgePlugin.FORGE_ACCOUNT_ACCOUNT_ID"
    private val FORGE_ACCOUNT_API_KEY = "ForgePlugin.FORGE_ACCOUNT_API_KEY"
    private val FORGE_ACCOUNT_FIRSTNAME = "ForgePlugin.FORGE_ACCOUNT_FIRSTNAME"
    private val FORGE_ACCOUNT_LASTNANE = "ForgePlugin.FORGE_ACCOUNT_LASTNANE"
    private val FORGE_ACCOUNT_EMAIL = "ForgePlugin.FORGE_ACCOUNT_EMAIL"
    private val CURRENT_IDEA_PROJECT_NAME = "ForgePlugin.CURRENT_IDEA_PROJECT_NAME"
    private val CURRENT_ISSUE = "ForgePlugin.CURRENT_ISSUE"
    private val CURRENT_FORGE_PROJECT_ID = "ForgePlugin.CURRENT_FORGE_PROJECT_ID"
    private val CURRENT_FORGE_PROJECT_NAME = "ForgePlugin.CURRENT_FORGE_PROJECT_NAME"

    private val TAG = "Account"

    private var account: Account? = null

    fun updateAccount(project: Project, user: User) {
        updateAccount(project, user, null, null, null)
    }

    fun updateAccount(project: Project, user: User, currentIssueId: String?, currentForgeProjectId: String?, currentForgeProjectName: String?) {
        this.account = Account(user, project.name, currentIssueId, currentForgeProjectId, currentForgeProjectName)
        save(project)
    }

    fun updateAccount(project: Project): Account? {
        /**
         * Обновление переменной account при смене проекта
         */
        account = load(project)
        return account
    }

    fun updateIssueId(project: Project, issue: Issue) {
        account?.let {
            it.currentIssueId = issue.id.toString()
            PropertiesComponent.getInstance(project).setValue(CURRENT_ISSUE, it.currentIssueId)
        }
    }

    fun getAccount(project: Project?): Account? {
//        if (project == null) return null
//        if (account == null || project.name != account?.currentIdeaProjectName) {
//            account = load(project)
//        }
        return account
    }

    fun getAccount(): Account? {
        return account
    }

    private fun save(project: Project?): Boolean {

        account?.let {
            with(it) {
                PropertiesComponent.getInstance().setValue(FORGE_ACCOUNT_ACCOUNT_ID, id)
                PropertiesComponent.getInstance().setValue(FORGE_ACCOUNT_API_KEY, apiKey)
                PropertiesComponent.getInstance().setValue(FORGE_ACCOUNT_FIRSTNAME, firstName)
                PropertiesComponent.getInstance().setValue(FORGE_ACCOUNT_LASTNANE, lastName)
                PropertiesComponent.getInstance().setValue(FORGE_ACCOUNT_EMAIL, email)
                if (project != null) {
                    PropertiesComponent.getInstance(project).setValue(CURRENT_IDEA_PROJECT_NAME, currentIdeaProjectName)
                    PropertiesComponent.getInstance(project).setValue(CURRENT_ISSUE, currentIssueId)
                    PropertiesComponent.getInstance(project).setValue(CURRENT_FORGE_PROJECT_ID, currentForgeProjectId)
                    PropertiesComponent.getInstance(project).setValue(CURRENT_FORGE_PROJECT_NAME, currentForgeProjectName)
                } else {
                    PropertiesComponent.getInstance().setValue(CURRENT_ISSUE, currentIssueId)
                    PropertiesComponent.getInstance().setValue(CURRENT_FORGE_PROJECT_ID, currentForgeProjectId)
                    PropertiesComponent.getInstance().setValue(CURRENT_FORGE_PROJECT_NAME, currentForgeProjectName)
                }
                return true
            }
        }
        return false
    }

    private fun load(project: Project?): Account? {
        val id = PropertiesComponent.getInstance().getValue(FORGE_ACCOUNT_ACCOUNT_ID)
        val apiKey = PropertiesComponent.getInstance().getValue(FORGE_ACCOUNT_API_KEY)

        val firstName = PropertiesComponent.getInstance().getValue(FORGE_ACCOUNT_FIRSTNAME) ?: ""
        val lastName = PropertiesComponent.getInstance().getValue(FORGE_ACCOUNT_LASTNANE) ?: ""
        val email = PropertiesComponent.getInstance().getValue(FORGE_ACCOUNT_EMAIL) ?: ""

        val currentIdeaProjectName: String
        val currentIssueId: String?
        val currentForgeProjectId: String?
        val currentForgeProjectName: String?

        if (project == null) {
            currentIdeaProjectName = ""
            currentIssueId = getCurrentIssueId()
            currentForgeProjectId = getCurrentForgeProjectId()
            currentForgeProjectName = getCurrentForgeProjectName()
        } else {
            currentIdeaProjectName = PropertiesComponent.getInstance(project).getValue(CURRENT_IDEA_PROJECT_NAME) ?: project.name
            currentIssueId = getCurrentIssueIdByIdeaProject(project)
            currentForgeProjectId = getCurrentForgeProjectIdByIdeaProject(project)
            currentForgeProjectName = getCurrentForgeProjectNameByIdeaProject(project)
        }

        if (apiKey != null && id != null) {
            val acc = Account(id, apiKey, firstName, lastName, email, currentIdeaProjectName, currentIssueId, currentForgeProjectId, currentForgeProjectName)
            MyLogger.info(TAG + " load() " + acc.toString())
            return acc
        }
        return null
    }

    private fun getCurrentIssueId() = PropertiesComponent.getInstance().getValue(CURRENT_ISSUE)

    private fun getCurrentForgeProjectId() = PropertiesComponent.getInstance().getValue(CURRENT_FORGE_PROJECT_ID)

    private fun getCurrentForgeProjectName() = PropertiesComponent.getInstance().getValue(CURRENT_FORGE_PROJECT_NAME)

    private fun getCurrentIssueIdByIdeaProject(project: Project) = PropertiesComponent.getInstance(project).getValue(CURRENT_ISSUE)

    private fun getCurrentForgeProjectIdByIdeaProject(project: Project) = PropertiesComponent.getInstance(project).getValue(CURRENT_FORGE_PROJECT_ID)

    private fun getCurrentForgeProjectNameByIdeaProject(project: Project) = PropertiesComponent.getInstance(project).getValue(CURRENT_FORGE_PROJECT_NAME)

    private fun getCurrentIssueObject(): Issue? {
        return IssuesManager.issue
    }

}