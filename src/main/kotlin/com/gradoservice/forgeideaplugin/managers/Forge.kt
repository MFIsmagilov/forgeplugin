package com.gradoservice.forgeideaplugin.managers

import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.requests.ForgeService
import com.intellij.ide.util.PropertiesComponent

object Forge {

    init {
        getForgeApiKey()?.let { init(it) }
    }

    private val FORGE_API_KEY = "ForgePlugin.FORGE_API_KEY"

    fun init(forgeApiKey: String) {
        apiKey = forgeApiKey
        service = ForgeService(apiKey = forgeApiKey)
    }

    fun saveForgeApiKey(forgeApiKey: String) {
        PropertiesComponent.getInstance().setValue(FORGE_API_KEY, forgeApiKey)
    }

    fun getForgeApiKey(): String? {
        return PropertiesComponent.getInstance().getValue(FORGE_API_KEY)
    }


    //Forge Request
    private var apiKey: String? = null
    private var service: ForgeService? = null


    fun getForgeService(): ForgeService {
        MyLogger.info("getForgeService ${Thread.currentThread()}")
        val serv = service
        return if (serv != null) {
            serv
        } else {
            init(getForgeApiKey() ?: throw Exception("Нет API key"))
            service!! //не хорошо но...
        }
    }
}