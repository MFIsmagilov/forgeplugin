package com.gradoservice.forgeideaplugin.managers

import com.gradoservice.forgeideaplugin.requests.StatusIssue
import com.taskadapter.redmineapi.bean.Issue


//надо как-то будет решить проблему с задачами
//а именно что текущая задача должна быть доступна хоть откуда
//сейчас это костылище
object IssuesManager {
    var issue: Issue? = null /* //todo: Открыть IDEA и сразу откртыо окно Commit Changes, issues = null
    */
//        get() {
//            if (field == null) {
//                IdeaUtils.getIdeaProject()?.let { project ->
//                    {
//
//                        Account.getCurrentIssueIdByIdeaProject(project)?.let {
//                            field = Forge.getForgeService().getIssue(it)
//                            field
//                        }
//                    }
//                }
//            }
//            return null
//        }

    fun load(id: String) {
        issue = Forge.getForgeService().getIssue(id)
    }

    fun swapAssignedAndStatus(newIssue: Issue?, assignedId: String) {
        this.issue?.let {
            it.assigneeId = null
            it.statusId = StatusIssue.FEEDBACK.id
            Forge.getForgeService().updateIssue(it)
        }
        newIssue?.let {
            it.assigneeId = Integer.valueOf(assignedId)
            it.statusId = StatusIssue.IN_WORK.id
            Forge.getForgeService().updateIssue(it)
        }
        this.issue = newIssue
    }

    fun clear() {
        issue = null
    }
}