package com.gradoservice.forgeideaplugin.gui.idea_forms

import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.gui.swing_forms.SettingsSwingForm
import com.gradoservice.forgeideaplugin.utils.IdeaUtils
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.gradoservice.forgeideaplugin.managers.Forge
import com.gradoservice.forgeideaplugin.models.Account
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.AppUIUtil
import com.taskadapter.redmineapi.bean.User
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JComponent

class SettingsForm(private val ideaProject: Project) : DialogWrapper(ideaProject) {

    private val TAG = "ForgePlugin.SettingsForm"

    private val settingSwingForm: SettingsSwingForm = SettingsSwingForm()

    private var user: User? = null //forge acc
    private var forgeProjects: MutableList<com.taskadapter.redmineapi.bean.Project>? = null
    private var account: Account? = null

    init {
        MyLogger.info("$TAG init ideaProject = ${ideaProject?.name}")
        title = "Forge Setting"
        setSavedParams()
        addMouseListenerProjectsCombobox()
        addMouseListenerTestConnection()
        init()
    }

    companion object {
        private val LOGGER: Logger = Logger.getInstance(SettingsForm.javaClass)
    }

    override fun createCenterPanel(): JComponent? {
        return this.settingSwingForm.rootPanel
    }


    override fun doOKAction() {
        try {
            user?.let {
                AccountManager.updateAccount(
                        ideaProject,
                        it,
                        null,
                        if (settingSwingForm.projectsComboBox.selectedItem is WrappedForgeProject)
                            (settingSwingForm.projectsComboBox.selectedItem as WrappedForgeProject).forgeProject.id.toString()
                        else
                            null,
                        if (settingSwingForm.projectsComboBox.selectedItem is WrappedForgeProject)
                            ((settingSwingForm.projectsComboBox.selectedItem as WrappedForgeProject).forgeProject.name)
                        else
                            null
                        )
            }
            close(-1, true)
        } catch (ex: Exception) {
            MyLogger.info("SettingsForm: ${ex.message}")
            IdeaUtils.showSuccessMessage(ideaProject, ex.message ?: "Exception message can be null?")
        }
    }

    private fun setSavedParams() {
        LOGGER.info("setSavedParams() ")
        account = AccountManager.getAccount(ideaProject)
        val acc = account
        LOGGER.info("setSavedParams() $acc")
        if (acc != null) {
            AppUIUtil.invokeOnEdt {
                settingSwingForm.forgeApiKeyTextField.text = acc.apiKey
                settingSwingForm.nameTextField.text = acc.firstName
                settingSwingForm.lastnameTextField.text = acc.lastName
                settingSwingForm.emailTextField.text = acc.email
                settingSwingForm.projectsComboBox.addItem(acc.currentForgeProjectName)
            }
        }
    }

    private fun addMouseListenerTestConnection() {

        settingSwingForm.authButton.addMouseListener(object : MouseAdapter() {
            override fun mouseClicked(e: MouseEvent?) {
                authWithProgress()
            }
        })
    }

    private fun addMouseListenerProjectsCombobox() {

        settingSwingForm.projectsComboBox.addMouseListener(object : MouseAdapter() {
//            override fun mouseClicked(e: MouseEvent?) {
//                authWithProgress()
//            }
        })
    }

    private fun setUpForge(forgeApiKey: String) {
        Forge.init(forgeApiKey)
        Forge.saveForgeApiKey(forgeApiKey)
    }

    private fun authAndSetParamsOnFrom() {
        setUpForge(settingSwingForm.forgeApiKeyTextField.text)
        user = Forge.getForgeService().getMyAccount()
        user?.let {
            account = Account(it, ideaProject.name)
            AppUIUtil.invokeOnEdt { settingSwingForm.setAccountParams(it.firstName, it.lastName, it.mail) }
        }
    }

    private fun loadProject() {
        account?.let {
            forgeProjects = Forge.getForgeService().getProjects()
            AppUIUtil.invokeOnEdt {
                settingSwingForm.projectsComboBox.removeAllItems()
                settingSwingForm.projectsComboBox.addItem("Choose project...")
                forgeProjects?.forEach {
                    settingSwingForm.projectsComboBox.addItem(WrappedForgeProject(it))
                }
            }
        }
    }

    private fun loadSuccess() {
//        settingSwingForm.projectsComboBox.isEnabled = true
    }

    private fun authWithProgress() {
        ProgressManager.getInstance().runProcessWithProgressSynchronously({
            try {
                ProgressManager.getInstance().progressIndicator.text = "Authorization..."
                authAndSetParamsOnFrom()
                ProgressManager.getInstance().progressIndicator.text = "Loading projects..."
                loadProject()
                loadSuccess()
            } catch (ex: Exception) {
                AppUIUtil.invokeOnEdt { IdeaUtils.showErrorMessage(ideaProject, "Error!\n${ex.message}") }
            }
        }, "Authorization on Forge", true, ideaProject)
    }


    class WrappedForgeProject(val forgeProject: com.taskadapter.redmineapi.bean.Project) {
        override fun toString(): String {
            return this.forgeProject.name
        }

        fun getProjectKey(): Int? {
            return forgeProject.id
        }
    }
}

