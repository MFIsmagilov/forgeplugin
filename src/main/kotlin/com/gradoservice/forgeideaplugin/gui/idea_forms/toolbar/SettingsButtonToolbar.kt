package com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar

import com.gradoservice.forgeideaplugin.ForgePluginIcons
import com.gradoservice.forgeideaplugin.gui.idea_forms.SettingsForm
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.Project


class SettingsButtonToolbar : AnAction("Settings", "Settings", ForgePluginIcons.GRADOSERVICE) {


    private var settingsForm: SettingsForm? = null
    private var currentIdeaProject: Project? = null

    override fun actionPerformed(e: AnActionEvent) {
//        val task = object : Task.Backgroundable(getEventProject(e), "Task1") {
//            override fun run(indicator: ProgressIndicator) {
//                indicator.text = "run"
//                Thread.sleep(5000)
//                indicator.text = "fwfr"
//            }
//        }
//        val progressIndicator = BackgroundableProcessIndicator(task)
//        ProgressManager.getInstance().runProcessWithProgressAsynchronously(task
//                , progressIndicator
//        )
//            ApplicationManager.getApplication().executeOnPooledThread {
//                ApplicationManager.getApplication().runReadAction {
//                }
//            }
        getEventProject(e)?.let {
            settingsForm = SettingsForm(it)
            settingsForm?.show()
        }
//        val statusBar = WindowManager.getInstance().getStatusBar(getEventProject(e))
    }

    override fun update(e: AnActionEvent?) {


        //todo: найти место получше
//        val project = getEventProject(e)
//        if (project != currentIdeaProject) {
//            currentIdeaProject = project
//            currentIdeaProject?.let {
//                AccountManager.updateAccount(it)
//                IssuesManager.clear()
//            }
//        }
    }
}