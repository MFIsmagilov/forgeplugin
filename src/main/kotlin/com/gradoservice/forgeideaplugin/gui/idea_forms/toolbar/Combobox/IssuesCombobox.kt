package com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar.Combobox

import com.gradoservice.forgeideaplugin.ForgePluginIcons
import com.gradoservice.forgeideaplugin.application_components.MyApplicationComponent
import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.actions.SearchTaskAction
import com.gradoservice.forgeideaplugin.extensions.formatDescriptionIssue
import com.gradoservice.forgeideaplugin.extensions.formatSubjectIssueLong
import com.gradoservice.forgeideaplugin.extensions.formatSubjectIssueShort
import com.gradoservice.forgeideaplugin.extensions.formattedToString
import com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar.IssueDoneAction
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.gradoservice.forgeideaplugin.managers.Forge
import com.gradoservice.forgeideaplugin.managers.IssuesManager
import com.gradoservice.forgeideaplugin.models.Account
import com.intellij.openapi.actionSystem.ex.ComboBoxAction
import com.intellij.openapi.project.Project
import com.taskadapter.redmineapi.bean.Issue
import javax.swing.JComponent
import com.intellij.openapi.actionSystem.*
import com.intellij.ui.AppUIUtil

open class IssuesCombobox : ComboBoxAction() {

    private var issues: List<Issue>? = null
    private var account: Account? = null
    private var ideaProject: Project? = null
    private var buttonCombobox: CustomComboboxButton = CustomComboboxButton(templatePresentation, null)
    private var defaultActionGroup = DefaultActionGroup()
    private var currentIssueAction = IssueAction(null, true)


    companion object {
        var TAG = "IssuesCombobox"
    }

    init {
        templatePresentation.text = "Issues by project"
        setPopupTitle("Forge")
    }

    override fun createPopupActionGroup(button: JComponent?): DefaultActionGroup {
        MyLogger.info("createPopupActionGroup")
//        updateAccount()
        if(account == null) updateAccount()
        account?.let {
            updatePopupActionGroup()
        }
        return defaultActionGroup
    }

    override fun createComboBoxButton(presentation: Presentation): ComboBoxButton {
        buttonCombobox = CustomComboboxButton(presentation, IssuesManager.issue)
        return buttonCombobox
    }


    private fun loadIssues() {
        account?.currentForgeProjectId?.let {
            MyLogger.info("$TAG loadIssue for $it")

            issues = Forge.getForgeService().getOpenIssues(it, null)
        }
    }

    override fun update(e: AnActionEvent?) {
        //Зашли в настройки и поменяли проект
        //нужно очистить combobobx
        MyApplicationComponent.getInstance().getFocused()?.let {
            if(ideaProject != it){
                MyLogger.info("$TAG поменялся проект обновляю toolbar")
                ideaProject = it
                updateAccount()

            }
        }
//        MyLogger.info("Update ${currentIssueAction.getIssueId()} и ${account?.currentIssueId}")
//        if (currentIssueAction.getIssueId() != account?.currentIssueId) {
//            updateCustomComboboxButton(null)
//            updateCurrentIssueAction(null)
//        }
//        val focusedProject = MyApplicationComponent.getInstance().getFocused()
//        if (focusedProject != ideaProject) {
//            ideaProject = focusedProject
//            globalUpdate()
//        }
    }

    private fun updateAccount() {
        val acc = AccountManager.getAccount()
        MyLogger.info("updateAccount \n $acc \n $account")
        if (account != acc) {
            MyLogger.info("Обновление аккаунта")
            account = acc?.clone()
            account?.currentIssueId?.let { IssuesManager.load(it) }
            globalUpdate()
        }
    }

    private fun updateIssueDoneAction(issue: Issue) {
        IssueDoneAction.getAction().reinit(issue)
        IssueDoneAction.getAction().enable()
    }

    private fun updateAccountIssueId(issue: Issue) {
        ideaProject?.let { AccountManager.updateIssueId(it, issue) }
    }

    private fun updatePopupActionGroup() {
        loadIssues()
        MyLogger.info("$TAG updatePopupActionGroup загрузил задачи")
        MyLogger.info("$TAG updatePopupActionGroup current = ${currentIssueAction}")
        val actionGroup = DefaultActionGroup()
        actionGroup.addSeparator("Current task")
        actionGroup.add(currentIssueAction)
        actionGroup.addSeparator("Find")
        actionGroup.add(SearchTaskAction(""))
        actionGroup.addSeparator("Tasks")
        issues?.forEach { issue ->
            run {
                MyLogger.info(issue.id.toString() + " " + issue.subject)
                actionGroup.add(IssueAction(issue))
            }
        }
        defaultActionGroup = actionGroup
    }

    private fun updateCustomComboboxButton(issue: Issue?) {
        if (issue == null) {
            buttonCombobox.clearButton()
        } else {
            buttonCombobox.updateButton(issue)
        }
    }

    private fun updateCurrentIssueAction(issue: Issue?) {
        currentIssueAction = if (issue == null) {
            IssueAction(null, true)
        } else {
            IssueAction(issue, true)
        }
    }

    private fun globalUpdate() {
        account?.let {
            IssuesManager.issue?.let { updateIssueDoneAction(it) }
            updateCustomComboboxButton(IssuesManager.issue)
            updateCurrentIssueAction(IssuesManager.issue)
//            updatePopupActionGroup()
        }
    }


    protected inner class CustomComboboxButton(private val presentation: Presentation, val issue: Issue?) : ComboBoxAction.ComboBoxButton(presentation) {

        init {
            if (issue != null) {
                updateButton(issue)
            } else {
                presentation.text = "Choose issue..."
                presentation.icon = ForgePluginIcons.REDMINE
            }
        }

        fun updateButton(issue: Issue) {
            presentation.text = issue.formatSubjectIssueShort()
            presentation.description = issue.formattedToString()
            presentation.icon = ForgePluginIcons.REDMINE
        }

        fun clearButton() {
            presentation.text = "Choose issue..."
        }

    }


    protected open inner class IssueAction(
            val issue: Issue?,
            val isCurrent: Boolean = false) : AnAction() {

        init {
            if (issue == null && isCurrent) {
                templatePresentation.text = "No current task"
            } else if (issue != null) {
                templatePresentation.text = issue.formatSubjectIssueLong()
                templatePresentation.description = issue.formattedToString()
                templatePresentation.icon = ForgePluginIcons.REDMINE
            }
        }

        override fun actionPerformed(e: AnActionEvent) {
            if (this@IssuesCombobox.currentIssueAction !== this) {
                this@IssuesCombobox.currentIssueAction = this
            }
            MyLogger.info("IssueAction actionPerformed")
            account?.let { IssuesManager.swapAssignedAndStatus(issue, it.id) }
            if (issue != null) {
                AppUIUtil.invokeOnEdt {
                    IssuesManager.issue = issue
                    updateCustomComboboxButton(issue)
                    updateIssueDoneAction(issue)
                    updateAccountIssueId(issue)
                }
            }

        }

        fun getIssueId(): String? {
            return issue?.id?.toString()
        }
    }

}