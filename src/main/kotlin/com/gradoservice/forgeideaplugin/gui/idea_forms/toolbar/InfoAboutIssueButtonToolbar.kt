package com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar

import com.gradoservice.forgeideaplugin.ForgePluginIcons
import com.gradoservice.forgeideaplugin.utils.IdeaUtils
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.gradoservice.forgeideaplugin.managers.Forge
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.taskadapter.redmineapi.bean.Issue

//Выводи сообщение с информацией о задаче
//Убрал из toolbar
class InfoAboutIssueButtonToolbar : AnAction("Information about issue", "Information about issue", ForgePluginIcons.QUESTION) {

    companion object {
        fun getAction(): InfoAboutIssueButtonToolbar {
            return ActionManager.getInstance().getAction("ForgePlugin.InfoAboutIssueButtonToolbar") as InfoAboutIssueButtonToolbar
        }
    }

    override fun actionPerformed(e: AnActionEvent) {
        getEventProject(e)?.let { ideaProject ->
            {
                AccountManager.getAccount(ideaProject)?.currentIssueId?.let {
                    Forge.getForgeService().getIssue(it)?.let {
                        IdeaUtils.showInformationMessage(ideaProject, "#${it.id}", formatIssue(it))
                    }
                }
            }
        }
    }

    private fun formatIssue(issue: Issue): String {
        return """${issue.subject}
            |Добавил(а)  ${issue.authorName}
            |Статус:     ${issue.statusName}
            |Приоритет:  ${issue.priorityText}
            |Готовность: ${issue.doneRatio}
            |Описание:  ${issue.description}
        """.trimMargin()
    }
}