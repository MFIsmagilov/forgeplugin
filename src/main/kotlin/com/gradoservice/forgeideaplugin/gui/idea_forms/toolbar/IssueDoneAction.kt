package com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar

import com.gradoservice.forgeideaplugin.ForgePluginIcons
import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.extensions.notificationIdeaAboutCloseIssue
import com.gradoservice.forgeideaplugin.managers.Forge
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.taskadapter.redmineapi.bean.Issue

//Ставит задаче 100% выполнено
class IssueDoneAction : AnAction("Done ratio 100%", "Done ratio 100%", ForgePluginIcons.DONE) {

    private var issue: Issue? = null
    private var isEnabled: Boolean = false


    companion object {

        val TAG = "ForgrePlugin.IssueDoneAction"

        fun getAction(): IssueDoneAction {
            return ActionManager.getInstance().getAction("ForgePlugin.IssueDoneAction") as IssueDoneAction
        }
    }

    override fun actionPerformed(e: AnActionEvent) {
        MyLogger.info("$TAG actionPerformed")
        issue?.let {
            if (it.doneRatio < 100) {
                it.doneRatio = 100
                Forge.getForgeService().oneHundredPercentIssue(it)
            } else {
                it.statusId = 5 // закрыт //todo убрать хардкод
                Forge.getForgeService().closeIssue(it)
                getEventProject(e)?.let { project -> it.notificationIdeaAboutCloseIssue(project) }
            }
        }
    }

    override fun update(e: AnActionEvent?) {

        issue?.let {
            if (it.doneRatio < 100) {
                e?.presentation?.icon = ForgePluginIcons.DONE
                e?.presentation?.text = "100%"
            } else {
                e?.presentation?.icon = ForgePluginIcons.DONE_ALL
                e?.presentation?.text = "Close issue"
            }
        }
        e?.presentation?.isEnabled = isEnabled
    }

    fun reinit(issue: Issue) {
        this.issue = issue
    }

    fun disable() {
        isEnabled = false
    }

    fun enable() {
        isEnabled = true
    }


}