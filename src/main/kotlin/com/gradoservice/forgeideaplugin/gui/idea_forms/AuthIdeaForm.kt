package com.gradoservice.forgeideaplugin.gui.idea_forms

import com.gradoservice.forgeideaplugin.gui.swing_forms.auth.AuthSwingForm
import com.gradoservice.forgeideaplugin.managers.Forge
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.Messages

import javax.swing.*

class AuthIdeaForm(private val project: Project?) : DialogWrapper(project) {

    private val authSwingForm: AuthSwingForm

    private val apiKeyFromTextField: String
        get() = this.authSwingForm.apiKeyTextField.text

    init {
        title = "Authorization on Forge"
        authSwingForm = AuthSwingForm()
        init()
    }

    override fun getOKAction(): Action {
        return myOKAction
    }

    override fun doOKAction() {
        //todo: вынести в отедлиный thread?
        try {
            val key = apiKeyFromTextField
            Forge.init(key)
            Forge.saveForgeApiKey(key)
            val user = Forge.getForgeService().getMyAccount()
            Messages.showMessageDialog(project, "Success!", "You Have Successfully Logged in! " + user!!.fullName, Messages.getInformationIcon())
            super.doOKAction()
        } catch (ex: Exception) {
//            Messages.showMessageDialog(project, "Failure!", ex.message, Messages.getErrorIcon())
        }
    }


    override fun createCenterPanel(): JComponent? {
        return authSwingForm.rootPanel
    }

    override fun createDefaultActions() {
        super.createDefaultActions()
    }
}
