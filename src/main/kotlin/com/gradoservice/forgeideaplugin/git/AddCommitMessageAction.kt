package com.gradoservice.forgeideaplugin.git

import com.gradoservice.forgeideaplugin.ForgePluginIcons
import com.gradoservice.forgeideaplugin.extensions.formattedSubject
import com.gradoservice.forgeideaplugin.extensions.formattedToString
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.gradoservice.forgeideaplugin.managers.IssuesManager
import com.gradoservice.forgeideaplugin.models.Account
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.vcs.CommitMessageI
import com.intellij.openapi.vcs.VcsDataKeys
import com.intellij.openapi.vcs.changes.ui.CommitChangeListDialog
import com.intellij.openapi.vcs.ui.Refreshable
import org.jetbrains.annotations.Nullable

class AddCommitMessageAction : AnAction("Add subject to commit",
        "Add subject to commit",
        ForgePluginIcons.REDMINE) {

    override fun actionPerformed(actionEvent: AnActionEvent) {

        val commitPanel = getCommitPanel(actionEvent) ?: return

        if (commitPanel is CommitChangeListDialog) {
            val issue = IssuesManager.issue
            commitPanel.commitMessage = """|${issue?.formattedSubject()}
                                           |${commitPanel.commitMessage}""".trimMargin()
        }
    }

    @Nullable
    private fun getCommitPanel(@Nullable e: AnActionEvent?): CommitMessageI? {
        if (e == null) return null

        val data = Refreshable.PANEL_KEY.getData(e.dataContext)
        return if (data is CommitMessageI) {
            data
        } else {
            VcsDataKeys.COMMIT_MESSAGE_CONTROL.getData(e.dataContext)
        }
    }
}
