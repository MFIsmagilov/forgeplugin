package com.gradoservice.forgeideaplugin.extensions

import com.gradoservice.forgeideaplugin.utils.IdeaUtils
import com.intellij.openapi.project.Project
import com.taskadapter.redmineapi.bean.Issue

fun Issue.formattedToString(): String {


    return """$subject
            |
            |Добавил(а)  $authorName $createdOn. Обновлено $updatedOn
            |Статус:     $statusName
            |Приоритет:  $priorityText
            |Версия:     $targetVersion
            |Готовность: $doneRatio%
            |Описание:   $description
        """.trimMargin()

}

fun Issue.formattedSubject(): String {
    return  "#$id $subject"
}

fun Issue.notificationIdeaAboutCloseIssue(project: Project) {
    IdeaUtils.showBallonStatusBarMessage(
            project,
            """
                <html>Задача <a href="https://forge.gradoservice.ru/issues/${id}" target="blank">#${id}</a> закрыта</html>",
                """
    )
}


fun Issue.formatSubjectIssueLong(): String {
    return "#$id $subject"
}

fun Issue.formatSubjectIssueShort(): String {
    return "#" + id
}

fun Issue.formatDescriptionIssue(): String {
    return description
}