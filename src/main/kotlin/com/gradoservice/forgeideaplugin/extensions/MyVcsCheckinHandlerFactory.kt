package com.gradoservice.forgeideaplugin.extensions

import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.gui.idea_forms.toolbar.IssueDoneAction
import com.intellij.openapi.vcs.CheckinProjectPanel
import com.intellij.openapi.vcs.checkin.CheckinHandler
import com.intellij.openapi.vcs.checkin.VcsCheckinHandlerFactory
import com.intellij.openapi.vcs.VcsException
import com.intellij.openapi.vcs.changes.ui.CommitChangeListDialog
import git4idea.GitVcs
import java.awt.event.MouseListener
import javax.swing.JButton
import javax.swing.JMenu

open class MyVcsCheckinHandlerFactory : VcsCheckinHandlerFactory(GitVcs.getKey()) {

    companion object {
        val TAG = "MyVcsCheckinHandlerFactory"
    }


    init {
        MyLogger.info(TAG + " init ${key.name}")
    }



    override fun createVcsHandler(panel: CheckinProjectPanel?): CheckinHandler {

        MyLogger.info(TAG + " createVcsHandler " + panel?.commitMessage)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.rootPane?.contentPane)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.rootPane?.glassPane)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.rootPane)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.actionMap?.parent)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.componentPopupMenu?.label)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component)
        MyLogger.info(TAG + " createVcsHandler " + panel?.component?.components)
        MyLogger.info(TAG + " createVcsHandler is" + (panel is CommitChangeListDialog).toString())
        panel?.commitMessage.plus("dfdfsfsdds")
        MyLogger.info(TAG + " createVcsHandler печатаю компоненты" )

        panel?.component?.components?.forEach {
            MyLogger.info(TAG + " createVcsHandler " + it.name + " ${it.parent}")
        }
//        panel?.component?.componentPopupMenu?.get("ssdfsfsfsf")
//        panel?.component?.add(JButton("MARAT"))//здоровая кнопка вместо  Commit message
        return CompletionCheckinHandler(panel ?: throw Exception("MyVcsCheckinHandlerFactory"))
    }

    class CompletionCheckinHandler(private val panel: CheckinProjectPanel) : CheckinHandler() {

        val TAG_C = "CompletionCheckinHandler"
        override fun includedChangesChanged() {
            MyLogger.info(TAG_C + " includedChangesChanged ")

        }

        override fun checkinSuccessful() {
            MyLogger.info(TAG_C + " checkinSuccessful ")
            dispose()
        }

        override fun checkinFailed(exception: List<VcsException>?) {
            MyLogger.info(TAG_C + " checkinFailed ")
            dispose()
        }

        private fun dispose() {
        }
    }

}