package com.gradoservice.forgeideaplugin.models

import com.taskadapter.redmineapi.bean.Project
import com.taskadapter.redmineapi.bean.User
import java.util.*

class ForgeUser(
        val firstName: String,
        val lastName: String,
        val email: String,
        val apiKey: String
) {

    constructor(forgeUser: User) : this(
            forgeUser.firstName,
            forgeUser.lastName,
            forgeUser.mail,
            forgeUser.apiKey)


}

class ForgeIssue(
        val id: Int,
        val created_on: Date,
        val updated_on: Date,
        val closed_on: Date
){
    constructor(forgeIssue: ForgeIssue): this(
            forgeIssue.id,
            forgeIssue.created_on,
            forgeIssue.updated_on,
            forgeIssue.closed_on
    )
}

class ForgeProject(
        val id: Int,
        val name: String,
        val identifier: String,
        val description: String,
        val homepage: String,
        val created_on: Date,
        val updated_on: Date
){
    constructor(forgeproject: Project): this(
            forgeproject.id,
            forgeproject.name,
            forgeproject.identifier,
            forgeproject.description,
            forgeproject.homepage,
            forgeproject.createdOn,
            forgeproject.updatedOn
    )
}