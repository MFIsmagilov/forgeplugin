package com.gradoservice.forgeideaplugin.models

import com.gradoservice.forgeideaplugin.managers.IssuesManager
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.taskadapter.redmineapi.bean.Issue
import com.taskadapter.redmineapi.bean.User

data class Account(
        val id: String,
        val apiKey: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        var currentIdeaProjectName: String,
        var currentIssueId: String?,
        var currentForgeProjectId: String?,
        var currentForgeProjectName: String?
) {


    constructor(user: User, currentIdeaProjectName: String) : this(user, currentIdeaProjectName,null, null, null)

    constructor(user: User, currentIdeaProjectName: String, currentIssueId: String?, currentForgeProjectId: String?, currentForgeProjectName: String?) :
            this(
                    user.id.toString(),
                    user.apiKey,
                    user.firstName,
                    user.lastName,
                    user.mail,
                    currentIdeaProjectName,
                    currentIssueId,
                    currentForgeProjectId,
                    currentForgeProjectName)


    fun clone(): Account {
        return this.copy()
    }
}