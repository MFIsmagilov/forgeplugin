package com.gradoservice.forgeideaplugin.application_components

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.ApplicationComponent
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.WindowManager

@State(name = "ForgePlugin", storages = [Storage(id = "ForgePlugin", file = "ForgePlugin.xml" )])
class MyApplicationComponent : ApplicationComponent, PersistentStateComponent<WindowSettings> {
    private var state: WindowSettings? = null
    private var focused: Project? = null


    override fun initComponent() {
        if (state == null) {
            state = WindowSettings()
        }
        if (state!!.isIncludeRecent) {
            WindowManager.getInstance().findVisibleFrame()
        }

    }

    override fun disposeComponent() {

    }

    companion object {

        fun getInstance(): MyApplicationComponent {
            return ApplicationManager.getApplication().getComponent<MyApplicationComponent>(MyApplicationComponent::class.java)
        }
    }


    override fun getComponentName(): String {
        return ""
    }


    fun getFocused(): Project? {
        return focused
    }


    override fun getState(): WindowSettings? {
        return state
    }

    override fun loadState(WindowSettings: WindowSettings) {
        this.state = WindowSettings
    }


    fun setFocused(focusedBefore: Project) {
        this.focused = focusedBefore
    }
}