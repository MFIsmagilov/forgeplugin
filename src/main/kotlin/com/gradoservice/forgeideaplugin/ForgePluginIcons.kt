package com.gradoservice.forgeideaplugin

import com.intellij.openapi.util.IconLoader


interface ForgePluginIcons {
    companion object {
        val GRADOSERVICE = IconLoader.getIcon("/icons/gradoservice.png")
        val REDMINE = IconLoader.getIcon("/icons/redmine.png")

        val SETTINGS_WHITE = IconLoader.getIcon("/icons/settings_white.png")
        val SETTINGS = IconLoader.getIcon("/icons/settings_black.png")

        val QUESTION = IconLoader.getIcon("/icons/question.png")
        val DONE = IconLoader.getIcon("/icons/ic_done.png")
        val DONE_ALL = IconLoader.getIcon("/icons/ic_done_all.png")
        val DONE_1 = IconLoader.getIcon("/icons/ic_done.svg")
    }
}
