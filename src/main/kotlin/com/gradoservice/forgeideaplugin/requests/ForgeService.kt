package com.gradoservice.forgeideaplugin.requests

import com.gradoservice.forgeideaplugin.MyLogger
import com.taskadapter.redmineapi.IssueManager
import com.taskadapter.redmineapi.RedmineManager
import com.taskadapter.redmineapi.RedmineManagerFactory
import com.taskadapter.redmineapi.bean.Issue
import com.taskadapter.redmineapi.bean.IssueFactory
import com.taskadapter.redmineapi.bean.Project
import com.taskadapter.redmineapi.bean.User
import org.apache.http.client.methods.RequestBuilder.post
import java.util.concurrent.Executors

/**
 * 1 - Новый
 * 2 - Назначен
 * 9 - В работе
 * 4 - Обратная связь
 * 5 - Закрыт
 * 3 - Заблокирован
 * 12 - На рассмотрении
 */
enum class StatusIssue(val id: Int){
    NEW(1),
    APPOINTED(2),
    IN_WORK(9),
    FEEDBACK(4),
    CLOSED(5),
    BLOCKED(3),
    UNDER_CONSIDERATION(12)
}


//Класс отвечает за все запросы
class ForgeService(apiKey: String, url: String = "https://forge.gradoservice.ru/") {

    private var manager: RedmineManager = RedmineManagerFactory.createWithApiKey(url, apiKey)
    private var queryId: Int? = null // any

    fun getMyAccount(): User? {
        return manager.userManager.currentUser
    }

    fun getProjects(): MutableList<Project>? {
        return manager.projectManager.projects
    }

    fun getIssue(issueId: String): Issue? {
        return manager.issueManager.getIssueById(Integer.valueOf(issueId))
    }

    fun getIssues(projectKey: String, queryId: Int? = null): MutableList<Issue>? {
        return manager.issueManager.getIssues(projectKey, queryId)
    }

    fun getOpenIssues(projectKey: String, queryId: Int? = null): MutableList<Issue>? {
        //todo добавить id проекта
        return manager.issueManager.getIssues(mutableMapOf(
                "project_id" to projectKey,
                "status_id" to "open"
        )).results
    }

    fun closeIssue(issue: Issue) {
        /**
         * 1 - Новый
         * 2 - Назначен
         * 9 - В работе
         * 4 - Обратная связь
         * 5 - Закрыт
         * 3 - Заблокирован
         * 12 - На рассмотрении
         */
        issue.statusId = 5
        manager.issueManager.update(issue)
    }

    fun oneHundredPercentIssue(issue: Issue) {
        issue.doneRatio = 100
        manager.issueManager.update(issue)
    }

    fun swapAssignedAndStatus(oldIssue: Issue?, newIssue: Issue?, assignedId: String) {
        oldIssue?.let {
            it.assigneeId = null
            it.statusId = StatusIssue.FEEDBACK.id
            manager.issueManager.update(it)
        }
        newIssue?.let {
            it.assigneeId = Integer.valueOf(assignedId)
            it.statusId = StatusIssue.IN_WORK.id
            manager.issueManager.update(it)
        }
    }

    fun updateIssue(issue: Issue){
        manager.issueManager.update(issue)
    }
}