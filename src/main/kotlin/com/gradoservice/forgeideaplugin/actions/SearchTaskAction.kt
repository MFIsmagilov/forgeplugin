package com.gradoservice.forgeideaplugin.actions

import com.gradoservice.forgeideaplugin.gui.idea_forms.SettingsForm
import com.intellij.execution.Executor
import com.intellij.execution.RunnerAndConfigurationSettings
import com.intellij.icons.AllIcons
import com.intellij.ide.actions.SearchEverywhereAction
import com.intellij.ide.ui.laf.darcula.ui.TextFieldWithPopupHandlerUI
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.actionSystem.ex.CustomComponentAction
import com.intellij.ui.AnActionButton
import com.intellij.ui.SearchTextField
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UIUtil

import javax.swing.*
import javax.swing.border.Border
import javax.swing.border.CompoundBorder
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent

class SearchTaskAction(text: String) : AnAction("Для поиска начните вводить текст: "), CustomComponentAction {
    private val myComponent: JPanel
    private val myField: SearchTextField

    val text: String
        get() = myField.text

    init {
        myField = object : SearchTextField(true) {
            override fun preprocessEventForTextField(e: KeyEvent): Boolean {
                if (KeyEvent.VK_ENTER == e.keyCode || '\n' == e.keyChar) {
                    e.consume()
                    addCurrentTextToHistory()
                    actionPerformed(null)
                }
                return super.preprocessEventForTextField(e)
            }

            override fun onFocusLost() {
                addCurrentTextToHistory()
                actionPerformed(null)
            }

            override fun onFieldCleared() {
                actionPerformed(null)
            }
        }
        val border = myField.getBorder()
        val emptyBorder = JBUI.Borders.empty(3, 0, 2, 0)
        if (border is CompoundBorder) {
            if (!UIUtil.isUnderDarcula()) {
                myField.setBorder(CompoundBorder(emptyBorder, border.insideBorder))
            }
        } else {
            myField.setBorder(emptyBorder)
        }

        myField.setSearchIcon(AllIcons.Actions.Filter_small)
        myComponent = JPanel()
        val layout = BoxLayout(myComponent, BoxLayout.X_AXIS)
        myComponent.layout = layout
        if (text.isNotEmpty()) {
            val label = JLabel(text)
            //label.setFont(label.getFont().deriveFont(Font.ITALIC));
            label.foreground = if (UIUtil.isUnderDarcula()) UIUtil.getLabelForeground() else UIUtil.getInactiveTextColor()
            label.border = JBUI.Borders.emptyLeft(3)
            myComponent.add(label)
        }
        myComponent.add(myField)
        myField.setVisible(true)
    }

    override fun createCustomComponent(presentation: Presentation): JComponent {
        return myComponent
    }

    fun setTextFieldFg(inactive: Boolean) {
        myField.textEditor.foreground = if (inactive) UIUtil.getInactiveTextColor() else UIUtil.getActiveTextColor()
    }

    override fun actionPerformed(e: AnActionEvent?) {

    }
}

