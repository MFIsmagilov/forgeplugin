package com.gradoservice.forgeideaplugin.actions

import com.gradoservice.forgeideaplugin.MyLogger
import com.gradoservice.forgeideaplugin.managers.AccountManager
import com.gradoservice.forgeideaplugin.managers.Forge
import com.gradoservice.forgeideaplugin.models.Account
import com.intellij.dvcs.DvcsUtil
import com.intellij.dvcs.push.VcsPushAction
import com.intellij.dvcs.push.ui.VcsPushDialog
import com.intellij.dvcs.repo.Repository
import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.util.containers.ContainerUtil
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.wm.WindowManager
import com.intellij.ui.AppUIUtil
import com.intellij.ui.awt.RelativePoint

class PushAction : VcsPushAction() {

    private val TAG = "ForgePlugin.PushAction"

    override fun actionPerformed(e: AnActionEvent) {
        val project = e.getRequiredData(CommonDataKeys.PROJECT)
        val manager = VcsRepositoryManager.getInstance(project)
        val repositories = if (e.getData<Editor>(CommonDataKeys.EDITOR) != null)
            ContainerUtil.emptyList()
        else
            collectRepositories(manager, e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY))
        val selectedFile = DvcsUtil.getSelectedFile(project)

        val dialog = VcsPushDialog(project, DvcsUtil.sortRepositories(repositories),
                if (selectedFile != null) manager.getRepositoryForFile(selectedFile) else null)

        dialog.show()
        MyLogger.info("$TAG exitcode = ${dialog.exitCode}")
//        if (dialog.exitCode == OK_EXIT_CODE) {
        MyLogger.info("$TAG Поток для закрытия задачи ${AnAction.getEventProject(e)}")
        try {
            AnAction.getEventProject(e)?.let { project ->
                {
                    MyLogger.info("$TAG Проект: ${project.name}")
                    AccountManager.getAccount(project)?.currentIssueId?.let {
                        MyLogger.info("$TAG Issue Id: $it")
                        Forge.getForgeService().getIssue(it)?.let { issue ->
                            {
                                MyLogger.info("$TAG Загрузил issue c forge")
                                Forge.getForgeService().closeIssue(issue)
                                MyLogger.info("$TAG Закрыл issue c forge")
                                AppUIUtil.invokeOnEdt {
                                    val statusBar = WindowManager.getInstance().getStatusBar(project)
                                    JBPopupFactory.getInstance()
                                            .createHtmlTextBalloonBuilder("""<p>Задача <a href="https://forge.gradoservice.ru/issues/${issue.id}>${issue.id} успешно закрыта</p>""", MessageType.INFO, null)
                                            .setFadeoutTime(7000)
                                            .createBalloon()
                                            .show(RelativePoint.getCenterOf(statusBar.component),
                                                    Balloon.Position.atRight)
                                }
                            }
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            AppUIUtil.invokeOnEdt {
                val statusBar = WindowManager.getInstance().getStatusBar(project)
                JBPopupFactory.getInstance()
                        .createHtmlTextBalloonBuilder("""<p>Что-то пошло не так бля</p>""", MessageType.INFO, null)
                        .setFadeoutTime(7000)
                        .createBalloon()
                        .show(RelativePoint.getCenterOf(statusBar.component),
                                Balloon.Position.atRight)
            }
        }
    }

    private fun collectRepositories(vcsRepositoryManager: VcsRepositoryManager,
                                    files: Array<VirtualFile>?): Collection<Repository> {
        if (files == null) return emptyList()
        val repositories = ContainerUtil.newHashSet<Repository>()
        files.mapNotNullTo(repositories) { vcsRepositoryManager.getRepositoryForFile(it) }
        return repositories
    }

}