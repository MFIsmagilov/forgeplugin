package com.gradoservice.forgeideaplugin.utils

import com.gradoservice.forgeideaplugin.MyLogger
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.wm.WindowManager
import com.intellij.ui.awt.RelativePoint

import java.awt.*
import java.io.IOException
import java.net.URISyntaxException

object IdeaUtils {

    //не уверен что работает так как надо
    val ideaProject: Project
        get() {
            MyLogger.info("IdeaUtils" + " " + "getIdeaProject")
            return ProjectManager.getInstance().openProjects[0]
        }

    fun showSuccessMessage(project: Project, message: String) {
        Messages.showMessageDialog(project, message, "Success!", Messages.getInformationIcon())
    }

    fun showErrorMessage(project: Project, message: String) {
        Messages.showMessageDialog(project, message, "Failure!", Messages.getErrorIcon())
    }

    fun showInformationMessage(project: Project, title: String, message: String) {
        Messages.showMessageDialog(project, message, title, Messages.getInformationIcon())
    }

    fun showBallonStatusBarMessage(project: Project, message: String) {
        val statusBar = WindowManager.getInstance().getStatusBar(project)
        JBPopupFactory.getInstance()
                .createHtmlTextBalloonBuilder(message, MessageType.INFO) { e ->
                    if (Desktop.isDesktopSupported() && e != null) {
                        try {
                            Desktop.getDesktop().browse(e.url.toURI())
                        } catch (e1: IOException) {
                            e1.printStackTrace()
                        } catch (e1: URISyntaxException) {
                            e1.printStackTrace()
                        }

                    }
                }
                .setFadeoutTime(7000)
                .createBalloon()
                .show(RelativePoint.getCenterOf(statusBar.component), Balloon.Position.above)
    }
}

////                .show(RelativePoint.getCenterOf(statusBar.component),
////                        Balloon.Position.atLeft)
//        val balloonNotifications = NotificationGroup("Notification group", NotificationDisplayType.BALLOON, true)
//
//        val success = balloonNotifications.createNotification(
//                "<html>BALLOON Paste", "<a href=\"" + "https://forge.gradoservice.ru/issues/42594" + "\" target=\"blank\">Paste</a> successfully posted!</html>",
//                NotificationType.INFORMATION, NotificationListener.UrlOpeningListener(true))
//        Notifications.Bus.notify(success, getEventProject(e))
//
//
//        val balloonNotifications1 = NotificationGroup("Notification group", NotificationDisplayType.NONE, true)
//
//        val success1 = balloonNotifications.createNotification(
//                "<html>STICKY_BALLOON Paste", "<a href=\"" + "https://forge.gradoservice.ru/issues/42594" + "\" target=\"blank\">Paste</a> successfully posted!</html>",
//                NotificationType.INFORMATION, NotificationListener.UrlOpeningListener(true))
//        Notifications.Bus.notify(success1, getEventProject(e))
//
//        val balloonNotifications2 = NotificationGroup("Notification group", NotificationDisplayType.NONE, true)
//
//        val success2 = balloonNotifications2.createNotification(
//                "<html>NONE Paste", "<a href=\"" + "https://forge.gradoservice.ru/issues/42594" + "\" target=\"blank\">Paste</a> successfully posted!</html>",
//                NotificationType.INFORMATION, NotificationListener.UrlOpeningListener(true))
//        Notifications.Bus.notify(success2, getEventProject(e))