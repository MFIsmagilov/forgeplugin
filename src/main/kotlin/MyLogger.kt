package com.gradoservice.forgeideaplugin


import com.intellij.openapi.diagnostic.Logger

/**
 * Created by MaRaT on 2.02.2018.
 */

object MyLogger {

    val logger: Logger = Logger.getInstance("ForgePlugin")

    fun info(message: String){
        logger.info(message)
    }
}