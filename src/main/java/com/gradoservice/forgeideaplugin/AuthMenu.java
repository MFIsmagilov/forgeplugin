package com.gradoservice.forgeideaplugin;

import com.gradoservice.forgeideaplugin.gui.idea_forms.SettingsForm;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

@Deprecated
public class AuthMenu extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
//        final AuthIdeaForm form = new AuthIdeaForm(getEventProject(e));
//        form.show();
        final SettingsForm form1 = new SettingsForm(getEventProject(e));
        form1.show();
    }
}
